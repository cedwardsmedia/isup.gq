# Use Alpine Linux as the base image
FROM alpine:latest

# Install Nginx, PHP-FPM, and PHP CURL
RUN apk update
RUN apk --no-cache add nginx php83-fpm php83-curl

# Create directories for Nginx and PHP
RUN mkdir -p /run/nginx /var/log/nginx
RUN mkdir -p /run/php /var/log/php

# Add configuration files for Nginx and PHP-FPM
# You would need to add your own custom configuration files here
COPY nginx.conf /etc/nginx/http.d/default.conf
#COPY www.conf /etc/php83/php-fpm.d/www.conf

# Setup a directory for your website code
RUN mkdir -p /var/www/
RUN mkdir -p /var/www/assets
COPY assets /var/www/assets
COPY *.php /var/www/

# Expose ports (80 for HTTP)
EXPOSE 80

# Start Nginx and PHP-FPM
CMD ["sh", "-c", "php-fpm83; nginx -g 'daemon off;'"]