var timer = 0;
var mystatus = document.getElementById("status");

function start() {
    if (document.getElementById('url').value != '') {
        setTimeout(isup, 1000);
    }
}

function fadeout() {
    mystatus.className += " animated fadeOut";
    timer = setTimeout(reset, 1000);
}

function reset() {
    mystatus.className = "text-muted";
    mystatus.innerHTML = "&#xfeff;";
}

function clearinput() {
    document.getElementById('url').className = "form-control";
}

function isup() {
    clearTimeout(timer);
    var url = document.getElementById('url').value;
    document.title = "Is " + url + " up?"
    mystatus.className = "text-muted animated fadeIn";
    mystatus.innerHTML = '<i class="fas fa-spinner fa-pulse"></i>';
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);

            if (response.Status == "up") {
                mystatus.className = "animated fadeIn green-text";
                document.getElementById('url').className = "form-control green-text";
            } else if (response.Status == "down") {
                mystatus.className = "animated fadeIn red-text";
                document.getElementById('url').className = "form-control red-text";
            } else if (response.Message == "...seriously?") {
                mystatus.className = "animated fadeIn text-muted";
                document.getElementById('url').className = "form-control";
            }
            mystatus.innerHTML = response.Message;
            document.getElementById('ad').className += 'lead animated fadeIn';
            timer = setTimeout(fadeout, 6000);
        }
        console.log(this.responseText);
        return;
    }
    xhttp.open("GET", "/" + url, true);
    xhttp.send();

}
window.onload = start();
