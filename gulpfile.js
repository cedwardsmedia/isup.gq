const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const minifycss = require('gulp-minify-css');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const php  = require('gulp-connect-php');
const git = require('gulp-git');
const bump = require('gulp-bump');
const gutil = require('gulp-util');

// Bump Prelease

gulp.task('bump:reset', function(){
    gulp.src('./package.json')
        .pipe(bump({version: '0.0.1'}))
        .pipe(gulp.dest('./'));

    gulp.src('./app/config.php')
        .pipe(bump({key: "VERSION = ", version: '0.0.1'}))
        .pipe(gulp.dest('./app'));
});

gulp.task('bump:pre', function(){
    gulp.src('./package.json')
        .pipe(bump({type:'prerelease'}))
        .pipe(gulp.dest('./'));

    gulp.src('./app/config.php')
        .pipe(bump({key: "VERSION = ", type:'prerelease'}))
        .pipe(gulp.dest('./app'));
});

// Bump Patch
gulp.task('bump:patch', function(){
    gulp.src('./package.json')
        .pipe(bump({type:'patch'}))
        .pipe(gulp.dest('./'));

    gulp.src('./app/config.php')
        .pipe(bump({key: "VERSION = ", type:'patch'}))
        .pipe(gulp.dest('./app'));
});

// Bump Minor
gulp.task('bump:minor', function(){
    gulp.src('./package.json')
        .pipe(bump({type:'minor'}))
        .pipe(gulp.dest('./'));

    gulp.src('./app/config.php')
        .pipe(bump({key: "VERSION = ", type:'minor'}))
        .pipe(gulp.dest('./app'));
});

// Bump Major
gulp.task('bump:major', function(){
    gulp.src('./package.json')
        .pipe(bump({type:'major'}))
        .pipe(gulp.dest('./'));

    gulp.src('./app/config.php')
        .pipe(bump({key: "VERSION = ", type:'major'}))
        .pipe(gulp.dest('./app'));

    gulp.run('vertag');
});

gulp.task('ver', function(){
    var carbon = require('./package.json');
    console.log(carbon.version);
});

// PHP Live Server
gulp.task('php', function() {
    php.server({
        router: 'index.php'
    });

    browserSync.init({
        proxy: '127.0.0.1:8000'
    });
    gulp.watch("*.php", {cwd: '.'}, browserSync.reload);
	gulp.watch("assets/scss/*.scss", ["sass", browserSync.reload]);

});

// Compile SASS
gulp.task("sass", function(){
    gulp.src("assets/scss/*.scss")
		.pipe(sass({ style: 'expanded' }))
					.pipe(autoprefixer("last 3 version","safari 5", "ie 8", "ie 9"))
		.pipe(gulp.dest("assets/css"))
		.pipe(rename({suffix: '.min'}))
		.pipe(minifycss())
		.pipe(gulp.dest('assets/css'));
});

gulp.task('dev', function() {
    gulp.start("php");
    gulp.start("sass")
});

gulp.task('deploy', function(){
  git.exec({args : 'push heroku'}, function (err, stdout) {
    if (err) throw err;
  });
});
