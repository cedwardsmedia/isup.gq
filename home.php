<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Is it up?</title>


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.8/css/mdb.min.css" rel="stylesheet">
    <!-- Roboto font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <style>
        html,
        body {
            height: 100%;
            align-content: center;
            font-family: 'Roboto', sans-serif;
        }

        body {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .blue-gradient {
            background: -webkit-linear-gradient(50deg, #45cafc, #303f9f) !important;

            background: -o-linear-gradient(50deg, #45cafc, #303f9f) !important;

            background: linear-gradient(40deg, #45cafc, #303f9f) !important;
        }

        footer {
            background-color: #f5f5f5;
        }

        .hidden {
            visibility: hidden;
        }
    </style>

    <!-- Begin OpenGraph -->
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Is it up?" />
    <meta property="og:description" content="Check if a website is up or down." />
    <meta property="og:url" content="https://isup.gq" />
    <meta property="og:site_name" content="Is it up?" />
    <meta property="fb:profile_id" content="cedwardsmedia" />
    <meta property="og:image" content="https://isup.gq/assets/images/social.png" />
    <!-- End OpenGraph -->



    <!-- Begin Twitter Card -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@cedwardsmedia" />
    <meta name="twitter:creator" content="@cedwardsmedia" />
    <meta name="twitter:title" content="Is it up?" />
    <meta name="twitter:description" content="Check if a website is up or down." />
    <meta name="twitter:image" content="https://isup.gq/assets/images/social.png" />
    <!-- End Twitter Card -->
</head>

<body class="text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 ">
                <div class="card animated zoomInUp">

                    <h5 class="card-header info-color white-text text-center py-4 blue-gradient">
<strong>Is it up?</strong>
</h5>

                    <!--Card content-->
                    <div class="card-body px-lg-5 pt-0">
                        <!-- Form -->
                        <form action="javascript:isup();" class="text-center" style="color: #757575;">
                            <!-- URL -->
                            <div class="md-form">
                                <input id="url" class="form-control" type="text" pattern="^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$" title="example.com" required onfocus="javascript:clearinput();" onkeypress="javascript:clearinput();" value="<?php if (isset($url)) {echo $url;} ?>">
                                <label for="url">Enter the URL of a website to check</label>
                                <span id="status">&#xfeff;</span>
                            </div>

                            <!-- Go button -->
                            <button class="btn btn-primary waves-effect waves-light btn-block" type="submit">Check</button>
                        </form>
                        <!-- Form -->
                    </div>
                </div>
            </div>
        </div>

        <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-md-6 ">
                    <p class="hidden" id="ad">
                        <?php include('ads.php'); ?>

                    </p>
                </div>
            </div>
        </div>

        <div class="fixed-bottom">
            <footer class="text-muted animated fadeInUp">
                &copy; 2018-<?= date('Y'); ?> <a href="https://www.cedwardsmedia.com/">Corey Edwards</a>
            </footer>
        </div>
    </div>

    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.8/js/mdb.min.js"></script>
    <!-- IsUp.gq JavaScript -->
    <script type="text/javascript" src="/assets/js/isupgq.js"></script>
</body>

</html>
