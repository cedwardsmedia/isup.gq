<?php

require_once('isup.php');

if (isset($_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_URI"] != '/'){
    $url = ltrim($_SERVER["REQUEST_URI"], '/');

    if (isset($_SERVER["HTTP_REFERER"])) {
        isup($url);
    } else {
        include('home.php');
    }
} else {
    include('home.php');
}
