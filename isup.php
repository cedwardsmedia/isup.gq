<?php

if(isset($_SERVER["HTTP_CF_CONNECTING_IP"])){
    $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_CF_CONNECTING_IP'];
}

function json($arr) {
    header('Content-Type: application/json');
    print_r(json_encode($arr, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT));
    echo ("\n");
    die();
}
function isup($url){
    if (isset($url)){

        $url = ltrim($_SERVER["REQUEST_URI"], '/');

        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $url = parse_url($url);
            $url = $url['host'];
        }


        if (strlen($url) < '4'){
            $arr = array('Status'=>"URL not defined or invalid.");
            header('Content-Type: application/json');
            print_r(json_encode($arr, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT));
            echo ("\n");
            die();
        }

        if (substr( $url, 0, 5 ) === "/http") {
            $url = ltrim(strtok($url, '?'), '/');
            $parse = parse_url($url);
            $url = $parse['host'];
        } else {
            $url = ltrim(strtok($url, '?'), '/');
        }

        if ($url == 'isup.gq') {
            $arr = array('Status'=>'up','HTTP Status'=>400,'Message'=>"😒...seriously?");
            json($arr);
        }

        function endswith($string, $test) {
            $strlen = strlen($string);
            $testlen = strlen($test);
            if ($testlen > $strlen) return false;
            return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;
        }

        $ch = curl_init($url);
        if (endswith($url, '.onion')) {
            curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:9100');
        }
        curl_setopt($ch,CURLOPT_USERAGENT,'IsUp.gq Checker/1.0 (+https://isup.gq)');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Forwarded-For: ' . $_SERVER['REMOTE_ADDR']));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if (200==$retcode) {
            $arr = array('Status'=>'up','HTTP Status'=>$retcode,'Message'=>"Yep. $url is up.");
        } else {
            if (0==$retcode) { $retcode = 'none'; }
            $arr = array('Status'=>'down','HTTP Status'=>$retcode,'Message'=>"Uh-oh! $url looks down from here too!");
        }
    } else {
        $arr = array('Status'=>"URL not defined or invalid.");
    }
    json($arr);
}


function get_browser_name($user_agent)
{
    if (!isset($user_agent)) return 'text';

    $user_agent = strtolower(ltrim(strtok($user_agent, '/')));
    if (strpos($user_agent, 'curl') !== false){
        return 'text';
    } elseif (strpos($user_agent, 'links') !== false) {
        return 'text';
    } elseif (strpos($user_agent, 'wget') !== false) {
        return 'text';
    } elseif (strpos($user_agent, 'emacs') !== false) {
        return 'text';
    } elseif (strpos($user_agent, 'lynx') !== false) {
        return 'text';
    } elseif (strpos($user_agent, 'browsh') !== false) {
        return 'text';
    } else {
        return 'html';
    }
}
